
public class BubbleSort {
    public static void sort(char[] sequence2){
        boolean isSorted = false;
        while(!isSorted){
            isSorted= true;
            for(int i = 0; i< sequence2.length-1; i++) {
                if (sequence2[i] > sequence2[i + 1]) {
                    isSorted = false;
                    char buf = sequence2[i];
                    sequence2[i] = sequence2[i + 1];
                    sequence2[i + 1] = buf;
                }
            }
        }

    }
}
