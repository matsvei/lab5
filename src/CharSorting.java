public class CharSorting {
    public static void main(String[] args) {
        char[] sequence1 = new char[]{ 2, 11, 3, 46, 18, 0, 21};
        char[] sequence2 = new char[]{2, 11, 3, 46, 18, 0, 21};
        char[] sequence3 = new char[]{2, 11, 3, 46, 18, 0, 21};
        SelectionSort.sort(sequence1);
        displaySelection(sequence1);
        BubbleSort.sort(sequence2);
        displayBubble(sequence2);
        InsertionSort.sort(sequence3);
        displayInsert(sequence3);
    }

    public static void displaySelection(char[] sequence1) {
        System.out.println("Selection: ");
        for (int element : sequence1) {
            System.out.print(element + " ");
        }
        System.out.println();
    }

    public static void displayBubble(char[] sequence2) {
        System.out.println("Bubble: ");
        for (int element : sequence2) {
            System.out.print(element + " ");
        }
        System.out.println();
    }
    public static void displayInsert(char[] sequence3){
        System.out.println("Insert: ");
        for (int element : sequence3) {
            System.out.print(element + " ");
        }
        System.out.println();
    }
}
