public class InsertionSort {
    public static void sort(char[] sequence3){
        for (int i = 0; i< sequence3.length; i++){
            char currentElement = sequence3[i];
            int previousKey = i-1;
            while (previousKey>=0 && sequence3[previousKey]>currentElement){
                sequence3[previousKey+1]= sequence3[previousKey];
                sequence3[previousKey]= currentElement;
                previousKey--;
            }
        }
    }
}