public class SelectionSort {
    public static void sort(char[] sequence1){
        for (int i= 0; i< sequence1.length; i++){
            int position=i;

            for (int j =i+1; j < sequence1.length;j++ ){
                if (sequence1[j]<sequence1[position]){
                    position=j;
                }
            }
            char temp = sequence1[position];
            sequence1[position]= sequence1[i];
            sequence1[i] = temp;
        }
    }

}